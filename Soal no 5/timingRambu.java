package timingRambu;
import java.util.Random;

class Lampu {
    int merah;
    int kuning;
    int hijau;
}
class Kendaraan {
    int[] jumlah = new int[2];
    String[] arah = {"Utara", "Selatan", "Barat", "Timur"};
}
public class timingRambu {
    public static void main(String[] args) {
        // Buat algoritma timing lampu merah perempatan dua arah secara otomatis berdasarkan jumlah kendaraan yang mengantri, 
        // visualisasikan secara interaktif!
        Lampu lampu = new Lampu();
        Kendaraan kendaraan = new Kendaraan();
        lampu.merah = 10; //waktu untuk 1 mobil adalah 10 detik
        Random rand = new Random();
        kendaraan.jumlah[0] = rand.nextInt(10);
        kendaraan.jumlah[1] = rand.nextInt(10);
        System.out.println("Jumlah kendaraan yang mengantri dari arah " + (kendaraan.arah[0]) + " adalah " + kendaraan.jumlah[0]);
        System.out.println("Jumlah kendaraan yang mengantri dari arah " + (kendaraan.arah[1]) + " adalah " + kendaraan.jumlah[1]);
        if(kendaraan.jumlah[0] > kendaraan.jumlah[1]) {
            System.out.println("Lampu merah akan menyala selama " + kendaraan.jumlah[0] * lampu.merah + " detik pada arah " + kendaraan.arah[2] + " dan " + kendaraan.arah[3]);
        } else {
            System.out.println("Lampu merah akan menyala selama " + kendaraan.jumlah[1] * lampu.merah + " detik pada arah " + kendaraan.arah[2] + " dan " + kendaraan.arah[3]);
        }
    }
}